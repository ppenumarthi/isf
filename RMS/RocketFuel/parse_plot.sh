#!/bin/bash

for i in $( ls *rocket*_grep*.rms* ); do
    #echo $i;
    max_val=0;
    for l in `seq 1 100`; do
        m=`grep "($l)" $i | wc -l`
        #echo $l $m $max_val
        if [ $m -gt 0 ]; then
          max_val=$l;
        fi
    done
    echo $i $max_val
    for l in $(seq 1 $max_val); do
        m=`grep "($l)" $i | wc -l`
        echo $l $m >>$i.processed
    done
done

#for i in $(seq $END); do echo $i; done
#for i in $(seq $END); do echo $i; done
