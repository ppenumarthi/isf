# Note you need gnuplot 4.4 for the pdfcairo terminal.

set terminal pdfcairo font "Gill Sans,9" linewidth 4 rounded fontscale 1.0

# Line style for axes
set style line 80 lt rgb "#808080"

# Line style for grid
set style line 81 lt 0  # dashed
set style line 81 lt rgb "#808080"  # grey

set grid back linestyle 81
set border 3 back linestyle 80 # Remove border on top and right.  These
             # borders are useless and make it harder
             # to see plotted lines near the border.
    # Also, put it in grey; no need for so much emphasis on a border.
set xtics nomirror
set ytics nomirror

#set log x
#set mxtics 10    # Makes logscale look good.

# Line styles: try to pick pleasing colors, rather
# than strictly primary colors or hard-to-see colors
# like gnuplot's default yellow.  Make the lines thick
# so they're easy to see in small plots in papers.
set style line 1 lt rgb "#A00000" lw 2 pt 1
set style line 2 lt rgb "#00A000" lw 2 pt 6
set style line 3 lt rgb "#5060D0" lw 2 pt 2
set style line 4 lt rgb "#B25900" lw 2 pt 9
set style line 5 lt rgb "#F46700" lw 2 pt 9
set style line 6 lt rgb "#825900" lw 2 pt 9

set output "all_rf.pdf"
set ylabel "Number of Sequences (links)"
set xlabel "Sequence Length"

set key top right

#set xrange [-1:11]
#set yrange [-80:-40]

plot "rocketfuel_1221_grep.rms.txt.processed" using 1:2 title "AS 1221" w lp ls 1, \
     "rocketfuel_1239_grep.rms.txt.processed" using 1:2 title "AS 1239" w lp ls 2, \
     "rocketfuel_1755_grep.rms.txt.processed" using 1:2 title "AS 1755" w lp ls 3, \
     "rocketfuel_3257_grep.rms.txt.processed" using 1:2 title "AS 3257" w lp ls 4, \
     "rocketfuel_3967_grep.rms.txt.processed" using 1:2 title "AS 3967" w lp ls 5, \
     "rocketfuel_6461_grep.rms.txt.processed" using 1:2 title "AS 6461" w lp ls 6     

set output "part_rf1.pdf"

set key top right

#set xrange [-1:11]
#set yrange [0:50]

plot "rocketfuel_1221_grep.rms.txt.processed" using 1:2 title "AS 1221" w lp ls 1, \
     "rocketfuel_1755_grep.rms.txt.processed" using 1:2 title "AS 1755" w lp ls 2, \
     "rocketfuel_3967_grep.rms.txt.processed" using 1:2 title "AS 3967" w lp ls 3, \
    
set output "part_rf2.pdf"

set key top right

#set xrange [-1:11]
#set yrange [0:50]

plot "rocketfuel_1239_grep.rms.txt.processed" using 1:2 title "AS 1239" w lp ls 1, \
     "rocketfuel_3257_grep.rms.txt.processed" using 1:2 title "AS 3257" w lp ls 2, \
     "rocketfuel_6461_grep.rms.txt.processed" using 1:2 title "AS 6461" w lp ls 3     
          
set output "all_rf_zoomed.pdf"

set key top right

#set xrange [-1:11]
set yrange [0:50]

plot "rocketfuel_1221_grep.rms.txt.processed" using 1:2 title "AS 1221" w lp ls 1, \
     "rocketfuel_1239_grep.rms.txt.processed" using 1:2 title "AS 1239" w lp ls 2, \
     "rocketfuel_1755_grep.rms.txt.processed" using 1:2 title "AS 1755" w lp ls 3, \
     "rocketfuel_3257_grep.rms.txt.processed" using 1:2 title "AS 3257" w lp ls 4, \
     "rocketfuel_3967_grep.rms.txt.processed" using 1:2 title "AS 3967" w lp ls 5, \
     "rocketfuel_6461_grep.rms.txt.processed" using 1:2 title "AS 6461" w lp ls 6
     
set output "part_rf1_zoomed.pdf"

set key top right

#set xrange [-1:11]
#set yrange [0:50]

plot "rocketfuel_1221_grep.rms.txt.processed" using 1:2 title "AS 1221" w lp ls 1, \
     "rocketfuel_1755_grep.rms.txt.processed" using 1:2 title "AS 1755" w lp ls 2, \
     "rocketfuel_3967_grep.rms.txt.processed" using 1:2 title "AS 3967" w lp ls 3, \
    
set output "part_rf2_zoomed.pdf"

set key top right

#set xrange [-1:11]
#set yrange [0:50]

plot "rocketfuel_1239_grep.rms.txt.processed" using 1:2 title "AS 1239" w lp ls 1, \
     "rocketfuel_3257_grep.rms.txt.processed" using 1:2 title "AS 3257" w lp ls 2, \
     "rocketfuel_6461_grep.rms.txt.processed" using 1:2 title "AS 6461" w lp ls 3     

