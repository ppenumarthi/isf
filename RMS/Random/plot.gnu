# Note you need gnuplot 4.4 for the pdfcairo terminal.

set terminal pdfcairo font "Gill Sans,9" linewidth 4 rounded fontscale 1.0

# Line style for axes
set style line 80 lt rgb "#808080"

# Line style for grid
set style line 81 lt 0  # dashed
set style line 81 lt rgb "#808080"  # grey

set grid back linestyle 81
set border 3 back linestyle 80 # Remove border on top and right.  These
             # borders are useless and make it harder
             # to see plotted lines near the border.
    # Also, put it in grey; no need for so much emphasis on a border.
set xtics nomirror
set ytics nomirror

#set log x
#set mxtics 10    # Makes logscale look good.

# Line styles: try to pick pleasing colors, rather
# than strictly primary colors or hard-to-see colors
# like gnuplot's default yellow.  Make the lines thick
# so they're easy to see in small plots in papers.
set style line 1 lt rgb "#A00000" lw 2 pt 1
set style line 2 lt rgb "#00A000" lw 2 pt 6
set style line 3 lt rgb "#5060D0" lw 2 pt 2
set style line 4 lt rgb "#B25900" lw 2 pt 9
set style line 5 lt rgb "#F46700" lw 2 pt 9
set style line 6 lt rgb "#825900" lw 2 pt 9


set ylabel "Number of Sequences (links)"
set xlabel "Sequence Length"

set output "part1_random.pdf"
set key top right

plot "BA25_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 25, Degree:4" w lp ls 1, \
     "BA25_6A.topo.rms.txt.processed" using 1:2 title "Nodes: 25, Degree:6" w lp ls 2, \
     "BA50_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 50, Degree:4" w lp ls 3, \
     "BA50_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 50, Degree:6" w lp ls 4, \
     "BA75_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 75, Degree:4" w lp ls 5, \
     "BA75_6A.topo.rms.txt.processed" using 1:2 title "Nodes: 75, Degree:6" w lp ls 6     

set output "part2_random.pdf"

set key top right

plot "BA100_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 100, Degree:4" w lp ls 1, \
     "BA100_6A.topo.rms.txt.processed" using 1:2 title "Nodes: 100, Degree:6" w lp ls 2, \
     "BA125_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 125, Degree:4" w lp ls 3, \
     "BA125_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 125, Degree:6" w lp ls 4, \

#set xrange [-1:11]
set yrange [0:10]

set output "part1_random_zoom.pdf"
set key top right

plot "BA25_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 25, Degree:4" w lp ls 1, \
     "BA25_6A.topo.rms.txt.processed" using 1:2 title "Nodes: 25, Degree:6" w lp ls 2, \
     "BA50_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 50, Degree:4" w lp ls 3, \
     "BA50_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 50, Degree:6" w lp ls 4, \
     "BA75_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 75, Degree:4" w lp ls 5, \
     "BA75_6A.topo.rms.txt.processed" using 1:2 title "Nodes: 75, Degree:6" w lp ls 6     


set output "part2_random_zoom.pdf"
set yrange [0:10]
set key top right

plot "BA100_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 100, Degree:4" w lp ls 1, \
     "BA100_6A.topo.rms.txt.processed" using 1:2 title "Nodes: 100, Degree:6" w lp ls 2, \
     "BA125_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 125, Degree:4" w lp ls 3, \
     "BA125_4A.topo.rms.txt.processed" using 1:2 title "Nodes: 125, Degree:6" w lp ls 4, \


