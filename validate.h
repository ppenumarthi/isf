#include "get_isf.h"


int no_of_nodes = 0;
int new_nextHop[MxSz][MxSz], old_nextHop[MxSz][MxSz];
int orig_cost[MxSz][MxSz], old_cost[MxSz][MxSz], new_cost[MxSz][MxSz];
int max_edge_cost = 0;

int edge_count = 0;

char buffer[MxSz];

int check_loop(char nodes_state[64], int source_node, int dest_node, int node1_with_failed_link, int node2_with_failed_link);

void validate_loops(int node1_for_failed_link, int node2_for_failed_link, int is_print);

void fifr_increasing_weights();

int get_dead_weight(int i, int j);

int get_edge_number(int source, int dest);

int get_edge_number(int source, int dest);
int get_source(int edge_number);
int get_dest(int edge_number);
