import sys

f=open(sys.argv[1], "r")
f_wr=open(sys.argv[1]+"_updated", "w")

#f.readline()
f_wr.write(f.readline())

while True:
    x=f.readline()
    if not x: break
    node1, node2, weigh = x.split()
    weight=float(weigh);
    weight=int(weight*10);
    f_wr.write(node1+ ' '+node2+' '+str(weight)+'\n');

f.close();
f_wr.close();
