import networkx as nx
import matplotlib.pyplot
import sys
#from networkx.drawing.nx_pydot import write_dot

print "This is the name of the script: ", sys.argv[0]
print "Number of arguments: ", len(sys.argv)
print "The arguments are: " , str(sys.argv)
print "The first argument is: " , sys.argv[1]

f=open(sys.argv[1], "r")
i=int(f.readline())
print i

g=nx.Graph()

'''
for j in xrange(0, i):
    print j
    g.add_node(j)
'''    

edge=0;

while True:
    x=f.readline()
    if not x: break
    print x
    edge=edge+1;
    node1, node2, weigh = x.split()
    g.add_edge(node1,node2,weight=weigh)
    
'''
g.node[j]['state']=j
g.edge[node1][node2]['state']=weigh
node_labels = nx.get_node_attributes(g,'state')
nx.draw_networkx_labels(g, pos, labels = node_labels)
edge_labels = nx.get_edge_attributes(g,'state')
nx.draw_networkx_edge_labels(g, pos, labels = edge_labels)
'''
print "Nodes::"
g.nodes()
print "Edges::", edge
g.edges()

'''
nx.draw(g)
nx.draw_random(g)
nx.draw_shell(g)
matplotlib.pyplot.show()
'''

pos = nx.spring_layout(g)

#nx.write_dot(g,'multi.dot')
nx.draw(g,pos, with_labels=True)
#node_labels = u for u in g.nodes(data=true)
#nx.draw_networkx_labels(g, pos, labels = node_labels)
edge_labels=dict([((u,v,),d['weight'])
             for u,v,d in g.edges(data=True)])
nx.draw_networkx_edge_labels(g,pos,edge_labels=edge_labels)


matplotlib.pyplot.savefig(sys.argv[1]+'.png', format="PNG", figsize=(1024, 768), dpi=1000, facecolor='w', edgecolor='k')
matplotlib.pyplot.show()
