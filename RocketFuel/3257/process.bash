cp $1 updated_weights.intra
cat updated_weights.intra | cut --d ' ' -f1 | sort -u >unique_nodes.intra
count=0;

while IFS= read -r line; do
  echo "$count: $line"
  count=$(($count+1));
  sed -i -e "s/$line/$count/g" updated_weights.intra
done < unique_nodes.intra 

sed -i "1i$count" updated_weights.intra
echo "-1">>updated_weights.intra
